// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public enum TipoErro {
	
	LEXICO("léxico"),
	SINTATICO("sintático"),
	SEMANTICO("semântico");
	
	private String name;
	
	private TipoErro(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}

// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

import java.util.ArrayList;
import java.util.List;

public class ErrorHandler {
	
	private static ErrorHandler instance;
	private List<Erro> listaDeErros;
	
	public static ErrorHandler getInstance() {
		if (instance == null) {
			instance = new ErrorHandler();
		}
		return instance;
	}

	private ErrorHandler() {
		this.listaDeErros = new ArrayList<Erro>();
	}
	
	public void registraErroLexico(String message, int lin, int col, String lexema) {
		listaDeErros.add(new Erro(message, lin, col, lexema, TipoErro.LEXICO));
	}
	
	public void registraErroSintatico(String message, int lin, int col, String lexema) {
		listaDeErros.add(new Erro(message, lin, col, lexema, TipoErro.SINTATICO));
	}

	public void registraErroSemantico(String message, int lin, int col, String lexema) {
		listaDeErros.add(new Erro(message, lin, col, lexema, TipoErro.SEMANTICO));
	}
	
	public void print() {
		System.out.println("*** Relatório de Erros ***");
		for (Erro e : listaDeErros) {
			System.out.println(e);
		}
		System.out.println();
		System.out.println("Total de Erros: " + listaDeErros.size());
		System.out.println("---");
		
	}
}

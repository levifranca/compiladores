// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AnLexico {

	private FileLoader fl;
	private StringBuilder lexema;
	private final static ErrorHandler eh = ErrorHandler.getInstance();
	private final static TabSimbolos ts = TabSimbolos.getInstance();

	public AnLexico(String filename) throws FileNotFoundException {
		fl = new FileLoader(filename);
	}

	public Token nextToken() throws IOException {
		Token t = null;
		do {
			try {
				t = q0();
			} catch (EOFException e) {
				t = novoToken(TokenCode.EOF);
			} catch (CompiladorException e) {
				eh.registraErroLexico(e.getMessage(), e.getLin(), e.getCol(),
						e.getLexema());
			}
		} while (t == null);
		
		return t;
	}

	private Token q0() throws IOException, CompiladorException {
		Token t;
		lexema = new StringBuilder();
		char c;
		do {
			c = fl.getNextChar();

		} while (Character.isWhitespace(c));

		if (Character.isDigit(c)) {
			lexema.append(c);
			t = q1();

		} else if (c == '\'') {
			lexema.append(c);
			t = q7();

		} else if (c == '_' || Character.isLetter(c)) {
			lexema.append(c);
			t = q8();

		} else if (c == '$') {
			lexema.append(c);
			t = q9();

		} else if (c == '+' || c == '-') {
			lexema.append(c);
			t = novoToken(TokenCode.ADDSUB_OP);

		} else if (c == '*' || c == '/') {
			lexema.append(c);
			t = novoToken(TokenCode.MULTDIV_OP);

		} else if (c == ';') {
			lexema.append(c);
			t = novoToken(TokenCode.TERM);

		} else if (c == '(') {
			lexema.append(c);
			t = novoToken(TokenCode.L_PAR);

		} else if (c == ')') {
			lexema.append(c);
			t = novoToken(TokenCode.R_PAR);

		} else if (c == ':') {
			lexema.append(c);
			t = q16();

		} else {
			lexema.append(c);
			throw new CompiladorException("Caracter inexperado.", fl.getLin(),
					fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q1() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			while (Character.isDigit(c)) {
				lexema.append(c);
				c = fl.getNextChar();
			}

			if (c == 'E') {
				lexema.append(c);
				t = q2();

			} else if (c == '.') {
				lexema.append(c);
				t = q4();

			} else { // [^\\.E0-9]
				fl.rollbackChar();
				t = novoToken(TokenCode.NUM_INT);

			}
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_INT);

		}
		return t;
	}

	private Token q2() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == '+' || c == '-') {
				lexema.append(c);
				t = q3();

			} else if (Character.isDigit(c)){
				fl.rollbackChar();
				t = q3();
				
			} else {
				lexema.append(c);
				throw new CompiladorException("Um '+', '-' ou dígito é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um '+' ou um '-' é experado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q3() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			
			if (!Character.isDigit(c)) {
				lexema.append(c);
				throw new CompiladorException("Um dígito é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
			
			while (Character.isDigit(c)) {
				lexema.append(c);
				c = fl.getNextChar();
			}
			// [^0-9]
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_INT);
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_INT);

		}
		return t;
	}

	private Token q4() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			
			if (!Character.isDigit(c)) {
				lexema.append(c);
				throw new CompiladorException("Um dígito é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
			
			while (Character.isDigit(c)) {
				lexema.append(c);
				c = fl.getNextChar();

			}

			if (c == 'E') {
				lexema.append(c);
				t = q5();

			} else { // [^E0-9]
				fl.rollbackChar();
				t = novoToken(TokenCode.NUM_FLOAT);
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_FLOAT);

		}
		return t;
	}

	private Token q5() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == '+' || c == '-') {
				lexema.append(c);
				t = q6();

			} else if (Character.isDigit(c)){
				fl.rollbackChar();
				t = q6();
				
			} else {
				lexema.append(c);
				throw new CompiladorException("Um '+', '-' ou dígito é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um '+' ou um '-' é experado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q6() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			
			if (!Character.isDigit(c)) {
				lexema.append(c);
				throw new CompiladorException("Um dígito é experado é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
			
			while (Character.isDigit(c)) {
				lexema.append(c);
				c = fl.getNextChar();

			}
			// [^0-9]
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_FLOAT);
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.NUM_FLOAT);

		}
		return t;
	}

	private Token q7() throws IOException {
		Token t;
		try {
			char c = fl.getNextChar();
			while (c != '\'') {
				lexema.append(c);
				c = fl.getNextChar();
			}
			// [']
			lexema.append(c);
			t = novoToken(TokenCode.LITERAL);
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.LITERAL);

		}
		return t;
	}

	private Token q8() throws IOException {
		Token t;
		try {
			char c = fl.getNextChar();
			while (c == '_' || Character.isLetter(c) || Character.isDigit(c)) {
				lexema.append(c);
				c = fl.getNextChar();

			}
			// [^_a-zA-Z0-9]
			fl.rollbackChar();
			t = novoToken(TokenCode.ID);
		} catch (EOFException e) {
			fl.rollbackChar();
			t = novoToken(TokenCode.ID);

		}
		
		if (ts.contem(lexema.toString())) {
			t = ts.retornaToken(lexema.toString());
			t = novoToken(t.getTokenCode());
		} else {
			t = ts.cadastraNovoToken(lexema.toString(), t);
		}
		return t;
	}

	private Token q9() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == 'd') {
				lexema.append(c);
				t = q10();

			} else if (c == 'l' || c == 'g') {
				lexema.append(c);
				t = q12();

			} else if (c == 'e') {
				lexema.append(c);
				t = q14();

			} else {
				lexema.append(c);
				throw new CompiladorException(
						"Um dos caracteres 'd', 'l', 'g' ou 'e' são experados.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException(
					"Um dos caracteres 'd', 'l', 'g' ou 'e' são experados.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q10() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == 'f') {
				lexema.append(c);
				t = q11();

			} else {
				lexema.append(c);
				throw new CompiladorException("Um 'f' é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um 'f' é experado.", fl.getLin(),
					fl.getCol(), lexema.toString());
		}
		return t;
	}

	// q13 e 15
	private Token q11() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == '$') {
				lexema.append(c);
				t = novoToken(TokenCode.REL_OP);

			} else {
				lexema.append(c);
				throw new CompiladorException("Um '$' é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um '$' é experado.", fl.getLin(),
					fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q12() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == 't' || c == 'e') {
				lexema.append(c);
				t = q11();

			} else {
				lexema.append(c);
				throw new CompiladorException("Um 't' ou um 'e' é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um 't' ou um 'e' é experado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q14() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == 'q') {
				lexema.append(c);
				t = q11();

			} else {
				lexema.append(c);
				throw new CompiladorException("Um 'q' é experado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um 'q' é experado.", fl.getLin(),
					fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q16() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c == '[') {
				lexema.append(c);
				t = q17();

			} else if (c == '=') {
				lexema.append(c);
				t = novoToken(TokenCode.ATTRIB_OP);

			} else {
				lexema.append(c);
				throw new CompiladorException("Um '[' ou um '=' é esperado.",
						fl.getLin(), fl.getCol(), lexema.toString());
			}
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Um '[' ou um '=' é esperado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q17() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			while (c != ']') {
				lexema.append(c);
				c = fl.getNextChar();
				 
			}
			// [\\]]
			lexema.append(c);
			t = q18();
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Fim de arquivo inexperado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token q18() throws IOException, CompiladorException {
		Token t;
		try {
			char c = fl.getNextChar();
			if (c != ':') {
				lexema.append(c);
				t = q17();

			}
			// [:]
			lexema.append(c);
			// Temos um comentario
			t = null;
		} catch (EOFException e) {
			fl.rollbackChar();
			throw new CompiladorException("Fim de arquivo inexperado.",
					fl.getLin(), fl.getCol(), lexema.toString());
		}
		return t;
	}

	private Token novoToken(TokenCode code) {
		return new Token(code, lexema.toString(), fl.getLin(), fl.getCol());
	}

}

// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

import java.util.HashMap;
import java.util.Map;

public class TabSimbolos {
	private static TabSimbolos instance;
	
	private Map<String, Token> tabela;
	
	private TabSimbolos() {
		tabela = new HashMap<String, Token>();
		loadKeywords();
	}
	

	public static TabSimbolos getInstance() {
		if (instance == null) {
			instance = new TabSimbolos();
		}
		return instance;
	}
	
	public Token cadastraNovoToken(String lexema, Token token) {
		if (tabela.containsKey(lexema)) {
			return tabela.get(lexema);
		}
		tabela.put(lexema, token);
		return token;
	}
	
	public boolean contem(String lexema) {
		return tabela.containsKey(lexema);
	}

	public Token retornaToken(String lexema) {
		return tabela.get(lexema);
	}
	
	private void loadKeywords() {
		Token t;
		
		t = new Token(TokenCode.LOGIC_VAL, "true");
		tabela.put("true", t);
		
		t = new Token(TokenCode.LOGIC_VAL, "false");
		tabela.put("false", t);
		
		t = new Token(TokenCode.LOGIC_OP, "not");
		tabela.put("not", t);
		
		t = new Token(TokenCode.LOGIC_OP, "and");
		tabela.put("and", t);
		
		t = new Token(TokenCode.LOGIC_OP, "or");
		tabela.put("or", t);
		
		t = new Token(TokenCode.TYPE, "bool");
		tabela.put("bool", t);
		
		t = new Token(TokenCode.TYPE, "text");
		tabela.put("text", t);
		
		t = new Token(TokenCode.TYPE, "int");
		tabela.put("int", t);
		
		t = new Token(TokenCode.TYPE, "float");
		tabela.put("float", t);
		
		t = new Token(TokenCode.PROGRAM, "program");
		tabela.put("program", t);
		
		t = new Token(TokenCode.END_PROG, "end_prog");
		tabela.put("end_prog", t);
		
		t = new Token(TokenCode.BEGIN, "begin");
		tabela.put("begin", t);

		t = new Token(TokenCode.END, "end");
		tabela.put("end", t);

		t = new Token(TokenCode.IF, "if");
		tabela.put("if", t);

		t = new Token(TokenCode.THEN, "then");
		tabela.put("then", t);

		t = new Token(TokenCode.ELSE, "else");
		tabela.put("else", t);

		t = new Token(TokenCode.FOR, "for");
		tabela.put("for", t);

		t = new Token(TokenCode.WHILE, "while");
		t.setLexema("while");
		tabela.put("while", t);

		t = new Token(TokenCode.DECLARE, "declare");
		tabela.put("declare", t);

		t = new Token(TokenCode.TO, "to");
		tabela.put("to", t);
		
	}
	
	public void print() {
		String format = "|%15s|%15s|%15s|%15s|\n";
		
		System.out.println();
		System.out.format("%45s\n", "*** Tabela de Simbolos ***");
		System.out.format(format, "Token Code", "Lexema", "Linha", "Coluna");
		for (Token t : tabela.values()) {
			if (TokenCode.ID.equals(t.getTokenCode())) {
				System.out.format(format, t.getTokenCode(), t.getLexema(), t.getLin(), t.getCol());
			}
		}
		System.out.println("---");
		System.out.println();
	}
}

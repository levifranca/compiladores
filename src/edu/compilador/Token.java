// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public class Token {
	private TokenCode tokenCode;
	private String lexema;
	private int lin;
	private int col;
	private boolean declarado = false;

	public Token(TokenCode tokenCode, String lexema) {
		this.tokenCode = tokenCode;
		this.lexema = lexema;
	}
	
	public Token(TokenCode tokenCode, String lexema, int lin, int col) {
		this.tokenCode = tokenCode;
		this.lexema = lexema;
		this.lin = lin;
		this.col = col;
	}
	
	public String getLexema() {
		return lexema;
	}

	public void setLexema(String lexema) {
		this.lexema = lexema;
	}

	public int getLin() {
		return lin;
	}

	public void setLin(int lin) {
		this.lin = lin;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public TokenCode getTokenCode() {
		return tokenCode;
	}

	public boolean isDeclarado() {
		return declarado;
	}

	public void setDeclarado(boolean declarado) {
		this.declarado = declarado;
	}

	@Override
	public String toString() {
		return "Token [ tokenCode = " + tokenCode + ", lexema = '" + (lexema == null ? "" : lexema) + "'  linha = " + lin + " , coluna = " + col + " , declarado = " + declarado + " ]";
	}
}

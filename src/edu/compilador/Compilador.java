// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public class Compilador {
	public static void main(String[] args) throws Exception {
		
		if (args.length < 1) {
			throw new IllegalArgumentException("Nome do arquivo não informado.");
		}

		String filename = args[0];

		AnSintatico as = new AnSintatico(filename);

		as.execute();
		
		TabSimbolos.getInstance().print();
		ErrorHandler.getInstance().print();
	}
	
}

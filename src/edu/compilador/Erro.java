// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public class Erro {
	private String message;
	private int lin;
	private int col;
	private String lexema;
	private TipoErro tipo;
	
	public Erro(String message, int lin, int col, String lexema, TipoErro tipo) {
		this.message = message;
		this.lin = lin;
		this.col = col;
		this.lexema = lexema;
		this.tipo = tipo;
	}

	public String getMessage() {
		return message;
	}

	public int getLin() {
		return lin;
	}

	public int getCol() {
		return col;
	}
	
	public String getLexema() {
		return lexema;
	}
	
	@Override
	public String toString() {
		return "Erro " + tipo.getName() + " na linha: " + lin + ", coluna: " + col + " e lexema: '" + lexema + "'. " + message;
	}
}

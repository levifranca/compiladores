// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public class CompiladorException extends Exception {

	private static final long serialVersionUID = 129811573848944986L;
	
	private String message;
	private int lin;
	private int col;
	private String lexema;
	
	public CompiladorException(String message, int lin, int col, String lexema) {
		this.message = message;
		this.lin = lin;
		this.col = col;
		this.lexema = lexema;
	}

	public String getMessage() {
		return message;
	}

	public int getLin() {
		return lin;
	}

	public int getCol() {
		return col;
	}
	
	public String getLexema() {
		return lexema;
	}
}

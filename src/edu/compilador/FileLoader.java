// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileLoader {
	private BufferedReader buffer;
	private int lin;
	private int col;
	boolean lineBreak = false;
	boolean tab = false;

	public FileLoader(String filename) throws FileNotFoundException {
		buffer = new BufferedReader(new FileReader(filename));
		lin = 1;
		col = 0;
	}

	public char getNextChar() throws IOException {
		buffer.mark(1);
		int i = buffer.read();
		if (i == -1)
			throw new EOFException();

		if (lineBreak) {
			lin++;
			col = 0;
			lineBreak = false;
		}
		
		char c = (char) i;
		if (c == '\n') {
			lineBreak = true;
		} else {
			if (c == '\t') {
				tab = true;
				col = col + 4;
			} else {
				tab = false;
				col++;
			}
		}
		
		return c;
	}

	public void rollbackChar() throws IOException {
		if (!lineBreak) {
			if (tab) {
				col = col - 4;
				tab = false;
			} else {
				col--;
			}
			
			if (col <= -1) {
				lin--;
			}
		} else {
			lineBreak = false;
		}
		buffer.reset();
	}

	public int getLin() {
		return lin;
	}

	public int getCol() {
		return col;
	}

}
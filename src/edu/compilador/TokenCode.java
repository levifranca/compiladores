// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

public enum TokenCode {
	
	NUM_INT,
	NUM_FLOAT,
	LITERAL,
	ID,
	REL_OP,
	ADDSUB_OP,
	MULTDIV_OP,
	ATTRIB_OP,
	TERM,
	L_PAR,
	R_PAR,
	LOGIC_VAL,
	LOGIC_OP,
	TYPE,
	PROGRAM,
	END_PROG,
	BEGIN,
	END,
	IF,
	THEN,
	ELSE,
	FOR,
	WHILE,
	DECLARE,
	TO,
	EOF
	
}

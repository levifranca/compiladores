// Fabio Eduardo Lima Amorim
// Gustavo Marcel Favarelli 
// Leonardo Leandro de Sousa
// Levi França

package edu.compilador;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AnSintatico {
	private AnLexico al;
	private Token tokenAtual;
	private boolean repeteToken;
	private boolean erroNoID;

	private final static TabSimbolos ts = TabSimbolos.getInstance();
	private final static ErrorHandler eh = ErrorHandler.getInstance();

	public AnSintatico(String filename) throws FileNotFoundException {
		al = new AnLexico(filename);
	}

	private void novoToken() throws IOException {
		if (repeteToken) {
			repeteToken = false;
			return;
		}

		tokenAtual = al.nextToken();
	}

	private void devolveToken() {
		repeteToken = true;
	}

	private boolean comparaToken(TokenCode tokenCode) {
		return tokenCode.equals(tokenAtual.getTokenCode());
	}

	private void registraErroSintatico(String message) {
		eh.registraErroSintatico(message, tokenAtual.getLin(),
				tokenAtual.getCol(), tokenAtual.getLexema());
	}
	
	private void registraErroSemantico(String message) {
		eh.registraErroSemantico(message, tokenAtual.getLin(),
				tokenAtual.getCol(), tokenAtual.getLexema());
	}

	public void execute() throws EOFException, IOException {
		do {
			S();
			novoToken();
		} while (tokenAtual.getTokenCode() != TokenCode.EOF);

	}

	private void S() throws IOException {
		// S ::= program id term BLOCO end_prog term
		novoToken();
		if (!comparaToken(TokenCode.PROGRAM)) {
			registraErroSintatico("O programa deve começar com 'program'.");
		}
		novoToken();
		if (!comparaToken(TokenCode.ID)) {
			registraErroSintatico("Um identificador para o programa era esperado.");
		}
		novoToken();
		if (!comparaToken(TokenCode.TERM)) {
			registraErroSintatico("Um ';' era esperado;");
		}
		BLOCO();
		novoToken();
		if (!comparaToken(TokenCode.END_PROG)) {
			registraErroSintatico("Um 'end_prog' era esperado.");
		}
		novoToken();
		if (!comparaToken(TokenCode.TERM)) {
			registraErroSintatico("Um ';' era esperado.");
		}
		return;
	}

	private void BLOCO() throws IOException {
		novoToken();
		if (!comparaToken(TokenCode.BEGIN)) {
			if (comparaToken(TokenCode.WHILE) || comparaToken(TokenCode.ID)
				|| comparaToken(TokenCode.FOR) || comparaToken(TokenCode.IF)
				|| comparaToken(TokenCode.DECLARE)) {
			// BLOCO ::= CMD
			CMD();
			}
		}

		if (erroNoID) {
			erroNoID = false;
			registraErroSintatico("Erro ao iniciar bloco.");
			devolveToken();
		}
		// BLOCO ::= begin CMDS end
		CMDS();
		novoToken();
		if (!comparaToken(TokenCode.END)) {
			registraErroSintatico("Um 'end' era esperado.");
		}
	}

	private void CMDS() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.WHILE)) {
			// CMDS ::= REPW CMDS
			REPW();
			CMDS();
		} else if (comparaToken(TokenCode.ID)) {
			// CMDS ::= id IDFLW
			regraSemantica1();
			IDFLW();
		} else if (comparaToken(TokenCode.FOR)) {
			// CMDS ::= REPF CMDS
			REPF();
			CMDS();
		} else if (comparaToken(TokenCode.IF)) {
			// CMDS ::= if IFFLW
			IFFLW();
		} else if (comparaToken(TokenCode.DECLARE)) {
			// CMDS ::= declare DCFLW
			DCFLW();
		} else {
			// CMDS ::= VAZIO
			devolveToken();
		}
	}

	private void IFFLW() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.L_PAR)) {
			// IFFLW ::= l_par EXPL r_par then BLOCO (CNDB) CMDS
			EXPL();
			novoToken();
			if (comparaToken(TokenCode.R_PAR)) {
				novoToken();
				if (comparaToken(TokenCode.THEN)) {
					BLOCO();
					CNDB();
					CMDS();
				} else {
					registraErroSintatico("Um 'then' era esperado.");
				}
			} else {
				registraErroSintatico("Um ')' era esperado.");
			}
		} else {
			registraErroSintatico("Um '(' era esperado.");
		}
	}

	private void IDFLW() throws IOException {
		novoToken();
		// IDFLW ::= attrib_op EXP term CMDS
		if (comparaToken(TokenCode.ATTRIB_OP)) {
			EXP();
		} else {
			registraErroSintatico("Um ':=' era esperado.");
		}
		novoToken();
		if (!comparaToken(TokenCode.TERM)) {
			registraErroSintatico("Um ';' era esperado.");
			while(!comparaToken(TokenCode.TERM)) novoToken();
		}
		CMDS();
	}

	private void DCFLW() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.ID)) {
			// DCFLW ::= id type term CMDS
			regraSemantica2();
			novoToken();
			if (!comparaToken(TokenCode.TYPE)) {
				registraErroSintatico("Um tipo de variavel era esperado.");
			}
		} else {
			registraErroSintatico("Um identificador era esperado.");
		}
		novoToken();
		if (!comparaToken(TokenCode.TERM)) {
			registraErroSintatico("Um ';' era esperado.");
			while(!comparaToken(TokenCode.TERM)) novoToken();
		}
		CMDS();
	}

	private void CMD() throws IOException {
		if (comparaToken(TokenCode.WHILE) || comparaToken(TokenCode.FOR)) {
			// CMD ::= REP
			REP();
		} else if (comparaToken(TokenCode.ID)) {
			// CMD ::= ATRIB
			ATRIB();
		} else if (comparaToken(TokenCode.IF)) {
			// CMD ::= COND
			COND();
		} else if (comparaToken(TokenCode.DECLARE)) {
			// CMD ::= DECL
			DECL();
		} else {
			registraErroSintatico("Um comando era esperado.");
		}

	}

	private void DECL() throws IOException {
		if (comparaToken(TokenCode.DECLARE)) {
			// DECL ::= declare id type term
			novoToken();
			if (comparaToken(TokenCode.ID)) {
				regraSemantica2();
				novoToken();
				if (comparaToken(TokenCode.TYPE)) {
					novoToken();
					if (comparaToken(TokenCode.TERM)) {
						// SUCESSO
					} else {
						registraErroSintatico("Um ';' era experado!");
					}
				} else {
					registraErroSintatico("Um tipo era experado!");
				}
			} else {
				registraErroSintatico("Um identificador era experado!");
			}
		} else {
			registraErroSintatico("Um 'declare' era experado!");
		}
	}

	private void COND() throws IOException {
		if (comparaToken(TokenCode.IF)) {
			// COND ::= if l_par EXPL r_par then BLOCO CNDB
			novoToken();
			if (comparaToken(TokenCode.L_PAR)) {
				EXPL();
				novoToken();
				if (comparaToken(TokenCode.R_PAR)) {
					novoToken();
					if (comparaToken(TokenCode.THEN)) {
						BLOCO();
						CNDB();
						// SUCESSO
					} else {
						registraErroSintatico("Um 'then' era experado.");
					}
				} else {
					registraErroSintatico("Um ')' era experado.");
				}
			} else {
				registraErroSintatico("Um '(' era experado.");
			}
		} else {
			registraErroSintatico("Um 'if' era experado.");
		}
	}

	private void CNDB() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.ELSE)) {
			// CNDB ::= else BLOCO
			BLOCO();
			// SUCESSO
		} else {
			// CNDB ::= VAZIO
			devolveToken();
		}
	}

	private void ATRIB() throws IOException {
		if (comparaToken(TokenCode.ID)) {
			// ATRIB ::= id attrib_op EXP term
			regraSemantica1();
			novoToken();
			if (comparaToken(TokenCode.ATTRIB_OP)) {
				EXP();
				novoToken();
				if (comparaToken(TokenCode.TERM)) {
					// SUCESSO
				} else {
					registraErroSintatico("Um ';' era experado.");
				}
			} else {
				erroNoID = true;
				registraErroSintatico("Um ':=' era experado.");
			}
		} else {
			registraErroSintatico("Um identificador era experado");
		}
	}

	private void EXP() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.L_PAR)) {
			// EXP ::= l_par EXPN r_par GENFLW1
			EXPN();
			novoToken();
			if (comparaToken(TokenCode.R_PAR)) {
				GENFLW1();
				// SUCESSO
			} else {
				registraErroSintatico("Um ')' era esperado.");
			}
		} else if (comparaToken(TokenCode.ID)) {
			// EXP ::= id GENFLW
			GENFLW();
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_FLOAT)) {
			// EXP ::= num_float GENFLW1
			GENFLW1();
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_INT)) {
			// EXP ::= num_int GENFLW1
			GENFLW1();
			// SUCESSO
		} else if (comparaToken(TokenCode.LOGIC_VAL)) {
			// EXP ::= logic_val LOGFLW
			LOGFLW();
			// SUCESSO
		} else if (comparaToken(TokenCode.LITERAL)) {
			// EXP ::= literal
			// SUCESSO
		} else {
			registraErroSintatico("Uma expressao era esperada.");
		}
	}

	private void EXPL() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.L_PAR)) {
			// EXPL ::= l_par EXPN r_par GENFLW1
			EXPN();
			novoToken();
			if (comparaToken(TokenCode.R_PAR)) {
				GENFLW1();
				// SUCESSO
			} else {
				registraErroSintatico("Um ')' era esperado.");
			}
		} else if (comparaToken(TokenCode.ID)) {
			// EXPL ::= id GENFLW
			GENFLW();
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_FLOAT)) {
			// EXPL ::= num_float GENFLW1
			GENFLW1();
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_INT)) {
			// EXPL ::= num_int GENFLW1
			GENFLW1();
			// SUCESSO
		} else if (comparaToken(TokenCode.LOGIC_VAL)) {
			// EXPL ::= logic_val LOGFLW
			LOGFLW();
			// SUCESSO
		} else {
			registraErroSintatico("Uma expressao era esperada.");
		}
	}

	private void LOGFLW() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.LOGIC_OP)) {
			// LOGFLW ::= logic_op EXPL
			EXPL();
			// SUCESSO
		} else {
			// LOGFLW ::= VAZIO
			devolveToken();
		}
	}

	private void GENFLW() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.LOGIC_OP)) {
			// GENFLW ::= logic_op EXPL
			EXPL();
		} else if (comparaToken(TokenCode.REL_OP)
				|| comparaToken(TokenCode.ADDSUB_OP)
				|| comparaToken(TokenCode.MULTDIV_OP)) {
			// GENFLW ::= GENFLW1
			devolveToken();
			GENFLW1();
			// SUCESSO
		} else {
			// GENFLW ::= VAZIO
			devolveToken();
		}
	}

	private void GENFLW1() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.REL_OP) || comparaToken(TokenCode.ADDSUB_OP)
				|| comparaToken(TokenCode.MULTDIV_OP)) {
			// GENFLW1 ::= TERMON1 EXPN1 GENFLW2
			TERMON1();
			EXPN1();
			GENFLW2();
			// SUCESSO
		} else {
			// GENFLW1 ::= VAZIO
			devolveToken();
		}
	}

	private void GENFLW2() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.REL_OP)) {
			// GENFLW2 ::= rel_op EXPN GENFLW3
			EXPN();
			GENFLW3();
			// SUCESSO
		} else {
			// GENFLW2 ::= VAZIO
			devolveToken();
		}
	}

	private void GENFLW3() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.LOGIC_OP)) {
			// GENFLW3 ::= logic_op EXPR
			EXPR();
			// SUCESSO
		} else {
			// GENFLW3 ::= VAZIO
			devolveToken();
		}
	}

	private void EXPR() throws IOException {
		// EXPR ::= EXPN rel_op EXPN
		EXPN();
		novoToken();
		if (comparaToken(TokenCode.REL_OP)) {
			EXPN();
			// SUCESSO
		} else {
			registraErroSintatico("Um '$' era esperado.");
		}
	}

	private void EXPN() throws IOException {
		// EXPN ::= TERMON EXPN1
		TERMON();
		EXPN1();
		// SUCESSO
	}

	private void EXPN1() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.ADDSUB_OP)) {
			// EXPN1 ::= addsub_op TERMON EXPN1
			TERMON();
			EXPN1();
			// SUCESSO
		} else {
			// EXPN1 ::= VAZIO
			devolveToken();
		}
	}

	private void TERMON() throws IOException {
		novoToken();
		if (comparaToken(TokenCode.ID) || comparaToken(TokenCode.L_PAR)
				|| comparaToken(TokenCode.NUM_FLOAT)
				|| comparaToken(TokenCode.NUM_INT)) {
			// TERMON ::= VALN TERMON1
			VALN();
			novoToken();
			TERMON1();
			// SUCESSO
		} else {
			registraErroSintatico("Um elemento de expressao era esperada.");
		}
	}

	private void TERMON1() throws IOException {
		if (comparaToken(TokenCode.MULTDIV_OP)) {
			// TERMON1 ::= multdiv_op VALN TERMON1
			novoToken();
			VALN();
			novoToken();
			TERMON1();
			// SUCESSO
		} else {
			// TERMON1 ::= VAZIO
			devolveToken();
		}
	}

	private void VALN() throws IOException {
		if (comparaToken(TokenCode.L_PAR)) {
			// VALN ::= l_par EXPN r_par
			EXPN();
			novoToken();
			if (comparaToken(TokenCode.R_PAR)) {
				// SUCESSO
			} else {
				registraErroSintatico("Um ')' era esperado.");
			}
		} else if (comparaToken(TokenCode.ID)) {
			// VALN ::= id
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_FLOAT)) {
			// VALN ::= num_float
			// SUCESSO
		} else if (comparaToken(TokenCode.NUM_INT)) {
			// VALN ::= num_int
			// SUCESSO
		} else {
			registraErroSintatico("Um valor era esperado.");
		}
	}

	private void REP() throws IOException {
		// REP ::= REPW
		if (comparaToken(TokenCode.WHILE)) {
			REPW();
			// SUCESSO
		} else if (comparaToken(TokenCode.FOR)) {
			// REP ::= REPF
			REPF();
			// SUCESSO
		} else {
			registraErroSintatico("Um loop era esperado (while/for).");
		}
	}

	private void REPF() throws IOException {
		// REPF ::= for id attrib_op EXPN to EXPN BLOCO
		if (comparaToken(TokenCode.FOR)) {
			novoToken();
			if (comparaToken(TokenCode.ID)) {
				novoToken();
				if (comparaToken(TokenCode.ATTRIB_OP)) {
					EXPN();
					novoToken();
					if (comparaToken(TokenCode.TO)) {
						EXPN();
						BLOCO();
						// SUCESSO
					} else {
						registraErroSintatico("Um 'to' era esperado");
					}
				} else {
					registraErroSintatico("Um '=' era esperado");
				}
			} else {
				registraErroSintatico("Uma variavel era esperada");
			}
		} else {
			registraErroSintatico("Um 'for' era esperado");
		}
	}

	private void REPW() throws IOException {
		// REPW ::= while l_par EXPL r_par BLOCO
		if (comparaToken(TokenCode.WHILE)) {
			novoToken();
			if (comparaToken(TokenCode.L_PAR)) {
				EXPL();
				novoToken();
				if (comparaToken(TokenCode.R_PAR)) {
					BLOCO();
					// SUCESSO
				} else {
					registraErroSintatico("Um ')' era esperado.");
				}
			} else {
				registraErroSintatico("Um '(' era esperado.");
			}
		} else {
			registraErroSintatico("Um 'while' era esperado.");
		}

	}
	
	private void regraSemantica1() {
		Token t = ts.retornaToken(this.tokenAtual.getLexema());
		if (!t.isDeclarado()) {
			registraErroSemantico("O identificador precisa ser declarado antes de ser atribuido.");
		}
	}
	
	private void regraSemantica2() {
		Token t = ts.retornaToken(this.tokenAtual.getLexema());
		if (t.isDeclarado()) {
			registraErroSemantico("Esse identificador já foi declarado.");
		} else {
			t.setDeclarado(true);
		}
	}
}
